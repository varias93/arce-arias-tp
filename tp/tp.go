package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

func createDatabase() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`drop database if exists amex`)
	if err != nil {
		log.Fatal(err)
	}
	
	_, err = db.Exec(`create database amex`)
	if err != nil {
		log.Fatal(err)
	}

}

func main() {
	var input int
	
	fmt.Printf("\n Ingrese la opcion que desea ejecutar \n 0 Crear la bases de datos, \n 1 Crear las tablas, \n 2 Especificar PK y Fk, \n 3 Cargar los datos, \n 4 Validar compras , \n 5 Generar resumen \n 6 Eliminar PK y Fk \n 7 Salir.\n")
	fmt.Scan(&input)

	if input == 0 {

		createDatabase()

		db, err := sql.Open("postgres", "user=postgres host=localhost dbname=amex sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}
		defer db.Close()
		main()
	}

	if input == 1 {
		db, err := sql.Open("postgres", "user=postgres host=localhost dbname=amex sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}
		defer db.Close()
	 _, err = db.Exec(`create table cliente (nrocliente int, nombre text, apellido text, domicilio text, telefono char(12));
					  create table tarjeta (nrotarjeta char(16), nrocliente int, validadesde char(6), validahasta char(6), codseguridad char(4), limitecompra decimal(8,2), estado char(10));
					  create table comercio (nrocomercio int, nombrec text, domicilio text, codigopostal char(8), telefono char(12));
					  create table compra(nrooperacion SERIAL, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), pagado boolean);
					  create table rechazo(nrorechazo SERIAL, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), motivo text);
					  create table alerta(nroalerta SERIAL, nrotarjeta char(16),fecha timestamp, nrorechazo int, codalerta int, descripcion text);
					  create table cierre(año int, mes int, terminacion int, fechainicio date, fechacierre date, fechavto date); 
					  create table cabecera(nroresumen int, nombre text, apellido text, domicilio text, nrotarjeta char(16), desde date, hasta date, vence date, total decimal(8,2));
					  create table detalle(nroresumen int, nrolinea int, fecha date, nombrecomercio text, monto decimal(7,2));
					  create table consumo(nrotarjeta char(16), codseguridad char(4), nrocomercio int, monto decimal(7,2));`)
		if err != nil {
			log.Fatal(err)
		}
		main()
	}

	if input == 2 {
		db, err := sql.Open("postgres", "user=postgres host=localhost dbname=amex sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}
		defer db.Close()
	_, err = db.Exec(`alter table cliente add constraint cliente_pk primary key (nrocliente);
					  alter table tarjeta add constraint tarjeta_pk primary key (nrotarjeta);
					  alter table comercio add constraint comercio_pk primary key (nrocomercio);
					  alter table compra add constraint compra_pk primary key (nrooperacion);
					  alter table rechazo add constraint rechazo_pk primary key (nrorechazo);
					  alter table cierre add constraint cierre_pk primary key (año,mes,terminacion);
					  alter table alerta add constraint alerta_pk primary key (nroalerta);
					  
					  alter table cabecera add constraint cabecera_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta (nrotarjeta);
					  alter table alerta add constraint alerta_nrorechazo_fk foreign key (nrorechazo) references rechazo (nrorechazo);
					  alter table tarjeta add constraint tarjeta_nrocliente_fk foreign key (nrocliente) references cliente (nrocliente);
					  alter table compra add constraint compra_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta (nrotarjeta);
					  alter table compra add constraint compra_nrocomercio_fk foreign key (nrocomercio) references comercio (nrocomercio);
					  alter table rechazo add constraint rechazo_nrocomercio_fk foreign key (nrocomercio) references comercio (nrocomercio);
					
					  `)
		if err != nil {
			log.Fatal(err)
		}

		_, err = db.Exec(`create or replace function autorizacion(nrotarjetac char(16), codseguridadc char(4), nrocomercioc int, montoc decimal(7,2)) returns boolean as $$
declare
    resultado record;
begin
select * into resultado from tarjeta where nrotarjeta = nrotarjetac;
    if not found or resultado.estado != 'vigente' then
        insert into rechazo values(DEFAULT,nrotarjetac,nrocomercioc,CURRENT_TIMESTAMP,montoc,'tarjeta no válida ó no vigente');
		return false;
	elsif resultado.codseguridad != codseguridadc then
		insert into rechazo values(DEFAULT,resultado.nrotarjeta,nrocomercioc,CURRENT_TIMESTAMP,montoc,'código de seguridad inválido');
		return false;
	elsif resultado.limitecompra<montoc then
		insert into rechazo values(DEFAULT,resultado.nrotarjeta,nrocomercioc,CURRENT_TIMESTAMP,montoc,'supera límite de tarjeta');
		return false;
	elsif resultado.validahasta< to_char(CURRENT_TIMESTAMP, 'YYYYMM') then
	insert into rechazo values(DEFAULT,resultado.nrotarjeta,nrocomercioc,CURRENT_TIMESTAMP, montoc,'plazo de vigencia expirado');
		return false;
	elsif resultado.estado='suspendida' then
        insert into rechazo values(DEFAULT,resultado.nrotarjeta,nrocomercioc,CURRENT_TIMESTAMP,montoc,'la tarjeta se encuentra suspendida');
        return false;
    else
        insert into compra values(DEFAULT,resultado.nrotarjeta,nrocomercioc,CURRENT_TIMESTAMP,montoc,false);
        return true;
        end if;
end;
$$ language plpgsql;


create or replace function generaralertarechazo() returns trigger as $$
begin
    insert into alerta values (DEFAULT, new.nrotarjeta, new.fecha, new.nrorechazo, 0, new.motivo);
    return new;
end;
$$ language plpgsql;

create trigger generar_alerta_trg
after insert on rechazo
for each row
execute procedure generaralertarechazo();


create or replace function generaralertarechazolimite() returns trigger as $$
declare
	oldr record;
begin
    select * into oldr from rechazo where nrotarjeta = new.nrotarjeta and motivo='supera límite de tarjeta' 
    and motivo=new.motivo and cast(fecha as date) = cast(new.fecha as date);
    if found then
        insert into alerta values (DEFAULT, new.nrotarjeta, new.fecha, new.nrorechazo, 32,'intento realizar mas de una compra que supera el limite de la tajeta');
		update tarjeta set estado= 'suspendida' where nrotarjeta= new.nrotarjeta;
    end if;
    return new;
end;

$$ language plpgsql;

create trigger generar_alerta_limite_trg
after insert on rechazo
for each row
execute procedure generaralertarechazolimite();


create or replace function generaralertacompra1min() returns trigger as $$
declare
    resultado record;
    codigo char(8);
begin
	select codigopostal into codigo from comercio where nrocomercio = new.nrocomercio;
    select * into resultado from compra c, comercio s where c.nrotarjeta = new.nrotarjeta and new.fecha - c.fecha < '1 min' and codigo = s.codigopostal and s.nrocomercio = c.nrocomercio;
    
    if found then
        insert into alerta values (DEFAULT, new.nrotarjeta, new.fecha, null, 1, 'Realizo 2 compras en menos de un minuto');
    end if;
    return new;
end;

$$ language plpgsql;

create trigger generar_alerta_min_trg
before insert on compra
for each row
execute procedure generaralertacompra1min();


create or replace function generaralertacompra5min() returns trigger as $$
declare
    resultado record;
    codigo char(8);
begin
	select codigopostal into codigo from comercio where nrocomercio = new.nrocomercio;
    select * into resultado from compra c, comercio s where c.nrotarjeta = new.nrotarjeta and new.fecha - c.fecha < '5 min' and codigo = s.codigopostal and s.nrocomercio != c.nrocomercio;
    
    if found then
        insert into alerta values (DEFAULT, new.nrotarjeta, new.fecha, null, 5, 'Realizo 2 compras en menos de 5 minuto en distintos cod postales');
    end if;
    return new;
end;

$$ language plpgsql;

create trigger generar_alerta_5min_trg
before insert on compra
for each row
execute procedure generaralertacompra5min();


create or replace function generarresumen(nroclientec int, periodo int) returns void as $$
	declare
	cab record;
	d record;
	tot decimal(8,2);
    serie int:=1;
	r int;
	
	begin		
	
	select max(nroresumen) into r from cabecera;
		if not found then
			r:=1;
		end if;
	
	for cab in select * from cliente c, tarjeta t, cierre s where c.nrocliente = nroclientec 
		and c.nrocliente = t.nrocliente and cast(right(t.nrotarjeta,1) as integer) = s.terminacion 
		and s.mes = periodo and t.estado='vigente' loop
		
		tot:=0;
			
		insert into cabecera values(r, cab.nombre, cab.apellido, cab.domicilio, cab.nrotarjeta, cab.fechainicio, cab.fechacierre, cab.fechavto, null);
		
		for d in select * from compra p, comercio m where cab.nrotarjeta=p.nrotarjeta 
		and m.nrocomercio=p.nrocomercio
		and cab.fechainicio <= cast(p.fecha as date) and cast(p.fecha as date) <= cab.fechacierre loop
			
			insert into detalle values (r,serie,d.fecha,d.nombrec,d.monto);
			update compra set pagado= true where nrotarjeta= cab.nrotarjeta;
		
			tot:=tot+d.monto;
			serie:=serie+1;
		
		end loop;
		update cabecera set total= tot where nrotarjeta= cab.nrotarjeta and vence=cab.fechavto;
		r:=r+1;
	end loop;
	
end;
$$ language plpgsql;`)
		if err != nil {
			log.Fatal(err)
		}
		main()
	}
	if input == 3 {
		db, err := sql.Open("postgres", "user=postgres host=localhost dbname=amex sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}
		defer db.Close()
		_, err = db.Exec(`insert into cliente values (1, 'Diego', 'Rodriguez','San Martin 545','011153456723');
					  insert into cliente values (2, 'Claudia','Herrera','San Martin 55','011198462713');
					  insert into cliente values (3, 'Romina','Cabrera','San Martin 5455','011123456124');
					  insert into cliente values (4, 'Belen','Morales','Roma 456','011194736286');
					  insert into cliente values (5, 'Ramiro','Molina','Atuel 7565','011121346723');
					  insert into cliente values (6, 'Stella','Ortiz','Malabia 1000','011134215987');
					  insert into cliente values (7, 'Hector','Ledesma','Cramer 695','011199954638');
					  insert into cliente values (8, 'Walter','Carrizo','Vilela 9860','011163524789');
					  insert into cliente values (9, 'Daniel','Cardozo','Arias 4520','011153455432');
					  insert into cliente values (10, 'Susana','Vazquez','Valdenegro 1520','011165478901');
					  insert into cliente values (11, 'Fernanda','Maldonado','Meller 1284','011106576723');
					  insert into cliente values (12, 'Ignacio','Rivero','Mariano Acha 374','011190213457');
					  insert into cliente values (13, 'Camila','Moyano','Lugones 3940','011153450946');
					  insert into cliente values (14, 'Juan','Valdez','Av. Congreso 1930','011153807123');
					  insert into cliente values (15, 'Laura','Torres','Av. Roca 1320','011179084352');
					  insert into cliente values (16, 'Yamila','Castro','Necochea 353','011150327685');
					  insert into cliente values (17, 'Karina','Rojas','Acassuso 2130','011130256784');
					  insert into cliente values (18, 'Jorge','Barrios','Ugarte 4684','011123094589');
					  insert into cliente values (19, 'Bautista','Flores','Monrroe 3244','011167345210');
					  insert into cliente values (20, 'Benjamin','Acosta','Quesada 4990','011176859012');
					  
					  insert into comercio values (1, 'Supermercado Argenchino','Av. Maipu 748','17445401','011657893411');
					  insert into comercio values (2, 'Supermercado Amanecer','Av. Moreno 878','16445741','011689853418');
					  insert into comercio values (3, 'Almacen Don Gabriel','Av. Mosconi 6542','17364245','011667886989');
					  insert into comercio values (4, 'Perfumeria Lilas','Av. Roca 978','17144511','011697863116');
					  insert into comercio values (5, 'Forrajeria Puppy','Belgrano 1243','15874514','011657856914');
					  insert into comercio values (6, 'Pescaderia El Salmon','Sarmiento 4567','16356871','011647297415');
					  insert into comercio values (7, 'Herreria HecThor','Junin 6123','15244852','011637897856');
					  insert into comercio values (8, 'Farmacia El Halcon','Italia 2874','54249354','011685893417');
					  insert into comercio values (9, 'Rockeria Lucifer','Centenera 1459','18754682','011627142418');
					  insert into comercio values (10, 'Casa de Pastas El ñoqui','Haedo 4235','17784721','011617841419');
					  insert into comercio values (11, 'YPF Hurlingham','Ruben Dario 496','14754734','011638513415');
					  insert into comercio values (12, 'Shell Moreno','Av. Victorica 852','17414372','011649853414');
					  insert into comercio values (13, 'Axion Energy San Miguel','Av. Mitre 2354','16572413','011675493413');
					  insert into comercio values (14, 'Dietetica El buen vivir','Av. Santa Fe 952','17524662','011625893412');
					  insert into comercio values (15, 'Burger King Terrazas','Las Magnolias 567','17471246','011614893411');
					  insert into comercio values (16, 'Mc Donalds Nine Shopping','España 521','17451432','011675893415');
					  insert into comercio values (17, 'Cinemark Malvinas','Tucuman 635','17826984','011664523412');
					  insert into comercio values (18, 'Village Cines Pilar','Corrientes 784','17313345','011645293415');
					  insert into comercio values (19, 'Tienda Otaku Kurama','Japon 3664','17454312','011674123414');
					  insert into comercio values (20, 'Insumos Informaticos VxV','Tribulato 2341','15624562','011675893416');
					 
					  insert into tarjeta values (4338645185721981,1,201706,202502,6589,40000.65,'vigente');
					  insert into tarjeta values (5258645145147981,1,201805,202603,6548,50000.95,'suspendida');
					  insert into tarjeta values (4770644723516981,2,201904,202512,7589,44110.85,'vigente');
					  insert into tarjeta values (4338645163146981,2,202001,202403,5887,57000.55,'vigente');
					  insert into tarjeta values (5258645178416981,3,201809,202304,6986,147000.85,'suspendida');
					  insert into tarjeta values (4770645187516354,4,201908,202608,9784,200000.65,'vigente');
					  insert into tarjeta values (4338645187514776,5,201707,202312,7889,478200.95,'anulada');
					  insert into tarjeta values (5258645187518931,6,201906,202401,6188,78000.35,'vigente');
					  insert into tarjeta values (4770645187513651,7,202002,202405,9685,87700.95,'vigente');
					  insert into tarjeta values (4338645187517892,8,201801,202201,8987,57400.85,'suspendida');
					  insert into tarjeta values (5258645187517642,9,201912,202605,5286,33400.95,'vigente');
					  insert into tarjeta values (4338645187517842,10,201910,202109,3492,28400.65,'anulada');
					  insert into tarjeta values (4770645187516350,11,201812,202206,2881,24000.75,'vigente');
					  insert into tarjeta values (5258645187514751,12,201810,202512,9685,47100.65,'suspendida');
					  insert into tarjeta values (4770645187517631,13,201809,202509,6888,89000.35,'vigente');
					  insert into tarjeta values (5258645187517831,14,201712,202407,4899,78100.45,'vigente');
					  insert into tarjeta values (4770645187515871,15,201711,202311,6389,48470.25,'anulada');
					  insert into tarjeta values (4338645187513654,16,201605,202310,9858,120065.65,'suspendida');
					  insert into tarjeta values (4770645187514765,17,201908,202308,3678,53980.65,'vigente');
					  insert into tarjeta values (5258645187516571,18,202005,202412,9852,97800.95,'vigente');
					  insert into tarjeta values (4770645187517849,19,201908,202306,4892,44100.65,'suspendida');
					  insert into tarjeta values (4338645187515471,20,201812,202404,9841,15487.65,'anulada');
					  
					  insert into cierre values(2020,1,0,'2020-01-02','2020-02-01','2020-02-10');
					  insert into cierre values(2020,1,1,'2020-01-03','2020-02-02','2020-02-11');
					  insert into cierre values(2020,1,2,'2020-01-04','2020-02-03','2020-02-12');
					  insert into cierre values(2020,1,3,'2020-01-05','2020-02-04','2020-02-13');
					  insert into cierre values(2020,1,4,'2020-01-06','2020-02-05','2020-02-14');
					  insert into cierre values(2020,1,5,'2020-01-07','2020-02-06','2020-02-15');
					  insert into cierre values(2020,1,6,'2020-01-08','2020-02-07','2020-02-16');
					  insert into cierre values(2020,1,7,'2020-01-09','2020-02-08','2020-02-17');
					  insert into cierre values(2020,1,8,'2020-01-10','2020-02-09','2020-02-18');
					  insert into cierre values(2020,1,9,'2020-01-11','2020-02-10','2020-02-19');

					  insert into cierre values(2020,2,0,'2020-02-02','2020-03-01','2020-03-10');
					  insert into cierre values(2020,2,1,'2020-02-03','2020-03-02','2020-03-11');
					  insert into cierre values(2020,2,2,'2020-02-04','2020-03-03','2020-03-12');
					  insert into cierre values(2020,2,3,'2020-02-05','2020-03-04','2020-03-13');
					  insert into cierre values(2020,2,4,'2020-02-06','2020-03-05','2020-03-14');
					  insert into cierre values(2020,2,5,'2020-02-07','2020-03-06','2020-03-15');
					  insert into cierre values(2020,2,6,'2020-02-08','2020-03-07','2020-03-16');
					  insert into cierre values(2020,2,7,'2020-02-09','2020-03-08','2020-03-17');
					  insert into cierre values(2020,2,8,'2020-02-10','2020-03-09','2020-03-18');
					  insert into cierre values(2020,2,9,'2020-02-11','2020-03-10','2020-03-19');					  

					  insert into cierre values(2020,3,0,'2020-03-02','2020-04-01','2020-04-10');
					  insert into cierre values(2020,3,1,'2020-03-03','2020-04-02','2020-04-11');
					  insert into cierre values(2020,3,2,'2020-03-04','2020-04-03','2020-04-12');
					  insert into cierre values(2020,3,3,'2020-03-05','2020-04-04','2020-04-13');
					  insert into cierre values(2020,3,4,'2020-03-06','2020-04-05','2020-04-14');
					  insert into cierre values(2020,3,5,'2020-03-07','2020-04-06','2020-04-15');
					  insert into cierre values(2020,3,6,'2020-03-08','2020-04-07','2020-04-16');
					  insert into cierre values(2020,3,7,'2020-03-09','2020-04-08','2020-04-17');
					  insert into cierre values(2020,3,8,'2020-03-10','2020-04-09','2020-04-18');
					  insert into cierre values(2020,3,9,'2020-03-11','2020-04-10','2020-04-19');						 
					  
					  insert into cierre values(2020,4,0,'2020-04-02','2020-05-01','2020-05-10');
					  insert into cierre values(2020,4,1,'2020-04-03','2020-05-02','2020-05-11');
					  insert into cierre values(2020,4,2,'2020-04-04','2020-05-03','2020-05-12');
					  insert into cierre values(2020,4,3,'2020-04-05','2020-05-04','2020-05-13');
					  insert into cierre values(2020,4,4,'2020-04-06','2020-05-05','2020-05-14');
					  insert into cierre values(2020,4,5,'2020-04-07','2020-05-06','2020-05-15');
					  insert into cierre values(2020,4,6,'2020-04-08','2020-05-07','2020-05-16');
					  insert into cierre values(2020,4,7,'2020-04-09','2020-05-08','2020-05-17');
					  insert into cierre values(2020,4,8,'2020-04-10','2020-05-09','2020-05-18');
					  insert into cierre values(2020,4,9,'2020-04-11','2020-05-10','2020-05-19');	
					  
					  insert into cierre values(2020,5,0,'2020-05-02','2020-06-01','2020-06-10');
					  insert into cierre values(2020,5,1,'2020-05-03','2020-06-02','2020-06-11');
					  insert into cierre values(2020,5,2,'2020-05-04','2020-06-03','2020-06-12');
					  insert into cierre values(2020,5,3,'2020-05-05','2020-06-04','2020-06-13');
					  insert into cierre values(2020,5,4,'2020-05-06','2020-06-05','2020-06-14');
					  insert into cierre values(2020,5,5,'2020-05-07','2020-06-06','2020-06-15');
					  insert into cierre values(2020,5,6,'2020-05-08','2020-06-07','2020-06-16');
					  insert into cierre values(2020,5,7,'2020-05-09','2020-06-08','2020-06-17');
					  insert into cierre values(2020,5,8,'2020-05-10','2020-06-09','2020-06-18');
					  insert into cierre values(2020,5,9,'2020-05-11','2020-06-10','2020-06-19');						  				  
					  
					  insert into cierre values(2020,6,0,'2020-06-02','2020-07-01','2020-07-10');
					  insert into cierre values(2020,6,1,'2020-06-03','2020-07-02','2020-07-11');
					  insert into cierre values(2020,6,2,'2020-06-04','2020-07-03','2020-07-12');
					  insert into cierre values(2020,6,3,'2020-06-05','2020-07-04','2020-07-13');
					  insert into cierre values(2020,6,4,'2020-06-06','2020-07-05','2020-07-14');
					  insert into cierre values(2020,6,5,'2020-06-07','2020-07-06','2020-07-15');
					  insert into cierre values(2020,6,6,'2020-06-08','2020-07-07','2020-07-16');
					  insert into cierre values(2020,6,7,'2020-06-09','2020-07-08','2020-07-17');
					  insert into cierre values(2020,6,8,'2020-06-10','2020-07-09','2020-07-18');
					  insert into cierre values(2020,6,9,'2020-06-11','2020-07-10','2020-07-19');						  
					  
					  insert into cierre values(2020,7,0,'2020-07-02','2020-08-01','2020-08-10');
					  insert into cierre values(2020,7,1,'2020-07-03','2020-08-02','2020-08-11');
					  insert into cierre values(2020,7,2,'2020-07-04','2020-08-03','2020-08-12');
					  insert into cierre values(2020,7,3,'2020-07-05','2020-08-04','2020-08-13');
					  insert into cierre values(2020,7,4,'2020-07-06','2020-08-05','2020-08-14');
					  insert into cierre values(2020,7,5,'2020-07-07','2020-08-06','2020-08-15');
					  insert into cierre values(2020,7,6,'2020-07-08','2020-08-07','2020-08-16');
					  insert into cierre values(2020,7,7,'2020-07-09','2020-08-08','2020-08-17');
					  insert into cierre values(2020,7,8,'2020-07-10','2020-08-09','2020-08-18');
					  insert into cierre values(2020,7,9,'2020-07-11','2020-08-10','2020-08-19');						    
					  
					  insert into cierre values(2020,8,0,'2020-08-02','2020-09-01','2020-09-10');
					  insert into cierre values(2020,8,1,'2020-08-03','2020-09-02','2020-09-11');
					  insert into cierre values(2020,8,2,'2020-08-04','2020-09-03','2020-09-12');
					  insert into cierre values(2020,8,3,'2020-08-05','2020-09-04','2020-09-13');
					  insert into cierre values(2020,8,4,'2020-08-06','2020-09-05','2020-09-14');
					  insert into cierre values(2020,8,5,'2020-08-07','2020-09-06','2020-09-15');
					  insert into cierre values(2020,8,6,'2020-08-08','2020-09-07','2020-09-16');
					  insert into cierre values(2020,8,7,'2020-08-09','2020-09-08','2020-09-17');
					  insert into cierre values(2020,8,8,'2020-08-10','2020-09-09','2020-09-18');
					  insert into cierre values(2020,8,9,'2020-08-11','2020-09-10','2020-09-19');						  
					  
					  insert into cierre values(2020,9,0,'2020-09-02','2020-10-01','2020-10-10');
					  insert into cierre values(2020,9,1,'2020-09-03','2020-10-02','2020-10-11');
					  insert into cierre values(2020,9,2,'2020-09-04','2020-10-03','2020-10-12');
					  insert into cierre values(2020,9,3,'2020-09-05','2020-10-04','2020-10-13');
					  insert into cierre values(2020,9,4,'2020-09-06','2020-10-05','2020-10-14');
					  insert into cierre values(2020,9,5,'2020-09-07','2020-10-06','2020-10-15');
					  insert into cierre values(2020,9,6,'2020-09-08','2020-10-07','2020-10-16');
					  insert into cierre values(2020,9,7,'2020-09-09','2020-10-08','2020-10-17');
					  insert into cierre values(2020,9,8,'2020-09-10','2020-10-09','2020-10-18');
					  insert into cierre values(2020,9,9,'2020-09-11','2020-10-10','2020-10-19');						  
					  
					  insert into cierre values(2020,10,0,'2020-10-02','2020-11-01','2020-11-10');
					  insert into cierre values(2020,10,1,'2020-10-03','2020-11-02','2020-11-11');
					  insert into cierre values(2020,10,2,'2020-10-04','2020-11-03','2020-11-12');
					  insert into cierre values(2020,10,3,'2020-10-05','2020-11-04','2020-11-13');
					  insert into cierre values(2020,10,4,'2020-10-06','2020-11-05','2020-11-14');
					  insert into cierre values(2020,10,5,'2020-10-07','2020-11-06','2020-11-15');
					  insert into cierre values(2020,10,6,'2020-10-08','2020-11-07','2020-11-16');
					  insert into cierre values(2020,10,7,'2020-10-09','2020-11-08','2020-11-17');
					  insert into cierre values(2020,10,8,'2020-10-10','2020-11-09','2020-11-18');
					  insert into cierre values(2020,10,9,'2020-10-11','2020-11-10','2020-11-19');						  
					  
					  insert into cierre values(2020,11,0,'2020-11-02','2020-12-01','2020-12-10');
					  insert into cierre values(2020,11,1,'2020-11-03','2020-12-02','2020-12-11');
					  insert into cierre values(2020,11,2,'2020-11-04','2020-12-03','2020-12-12');
					  insert into cierre values(2020,11,3,'2020-11-05','2020-12-04','2020-12-13');
					  insert into cierre values(2020,11,4,'2020-11-06','2020-12-05','2020-12-14');
					  insert into cierre values(2020,11,5,'2020-11-07','2020-12-06','2020-12-15');
					  insert into cierre values(2020,11,6,'2020-11-08','2020-12-07','2020-12-16');
					  insert into cierre values(2020,11,7,'2020-11-09','2020-12-08','2020-12-17');
					  insert into cierre values(2020,11,8,'2020-11-10','2020-12-09','2020-12-18');
					  insert into cierre values(2020,11,9,'2020-11-11','2020-12-10','2020-12-19');						  
					  
					  insert into cierre values(2020,12,0,'2020-12-02','2021-01-01','2021-01-10');
					  insert into cierre values(2020,12,1,'2020-12-03','2021-01-02','2021-01-11');
					  insert into cierre values(2020,12,2,'2020-12-04','2021-01-03','2021-01-12');
					  insert into cierre values(2020,12,3,'2020-12-05','2021-01-04','2021-01-13');
					  insert into cierre values(2020,12,4,'2020-12-06','2021-01-05','2021-01-14');
					  insert into cierre values(2020,12,5,'2020-12-07','2021-01-06','2021-01-15');
					  insert into cierre values(2020,12,6,'2020-12-08','2021-01-07','2021-01-16');
					  insert into cierre values(2020,12,7,'2020-12-09','2021-01-08','2021-01-17');
					  insert into cierre values(2020,12,8,'2020-12-10','2021-01-09','2021-01-18');
					  insert into cierre values(2020,12,9,'2020-12-11','2021-01-10','2021-01-19');
					  						  
					   
					  
					  insert into consumo values ('4770645187515871','6389',3,1902);
					  insert into consumo values ('258645187514751','9685',4,5500);
					  insert into consumo values ('5258645187517831','4899',10,80000);
					  insert into consumo values ('5258645187517831','4899',10,80000);
					  insert into consumo values ('5258645187516571','9851',17,1200);



					  insert into consumo values ('4770645187516350','2881',12,1000.4);
					  insert into consumo values ('4770645187517631','6888',3,234.5);
					  insert into consumo values ('5258645187517642','5286',2,1560.4);
					  insert into consumo values ('5258645187517642','5286',11,100.3);
					  
					  
					 
					  
					  `)
		if err != nil {
			log.Fatal(err)
		}
		main()
	}
	if input == 4 {
		db, err := sql.Open("postgres", "user=postgres host=localhost dbname=amex sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}	
		_, err = db.Exec(`
					  do $$
					  declare 
					  cons record;
					  begin
					  for cons in select * from consumo loop
							perform autorizacion(cons.nrotarjeta,cons.codseguridad,cons.nrocomercio,cons.monto);
					  end loop;
					  end;
					  $$ language plpgsql;`)
		if err != nil {
			log.Fatal(err)
		}
		main()
	}
	
	if input == 5 {
		db, err := sql.Open("postgres", "user=postgres host=localhost dbname=amex sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}	
		_, err = db.Exec(`select generarresumen(1,12);
		                  select pg_sleep(0.1);
						  select generarresumen(2,12);
						  select pg_sleep(0.1);
						  select generarresumen(3,12);
						  select pg_sleep(0.1);
						  select generarresumen(4,12);
						  select pg_sleep(0.1);
						  select generarresumen(5,12);
						  select pg_sleep(0.1);
						  select generarresumen(6,12);
						  select pg_sleep(0.1);
						  select generarresumen(7,12);
						  select pg_sleep(0.1);
						  select generarresumen(8,12);
						  select pg_sleep(0.1);
						  select generarresumen(9,12);
						  select pg_sleep(0.1);
						  select generarresumen(10,12);
						  select pg_sleep(0.1);
						  select generarresumen(11,12);
						  select pg_sleep(0.1);
						  select generarresumen(12,12);
						  select pg_sleep(0.1);
						  select generarresumen(13,12);
						  select pg_sleep(0.1);
						  select generarresumen(14,12);
						  select pg_sleep(0.1);
						  select generarresumen(15,12);
						  select pg_sleep(0.1);
						  select generarresumen(16,12);
						  select pg_sleep(0.1);
						  select generarresumen(17,12);
						  select pg_sleep(0.1);
						  select generarresumen(18,12);
						  select pg_sleep(0.1);
						  select generarresumen(19,12);
						  select pg_sleep(0.1);
						  select generarresumen(20,12);
						  	
						  `)
		if err != nil {
			log.Fatal(err)
		}
		main()
	}
	
	if input == 6 {
		db, err := sql.Open("postgres", "user=postgres host=localhost dbname=amex sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}	
		_, err = db.Exec(`alter table tarjeta DROP constraint tarjeta_nrocliente_fk;
						  alter table compra DROP constraint compra_nrotarjeta_fk;
						  alter table compra DROP constraint compra_nrocomercio_fk;
						  alter table rechazo DROP constraint rechazo_nrocomercio_fk;
						  alter table cabecera DROP constraint cabecera_nrotarjeta_fk;
						  alter table alerta DROP constraint alerta_nrorechazo_fk;
						  alter table cliente DROP constraint cliente_pk;
					      alter table tarjeta DROP constraint tarjeta_pk; 
					      alter table comercio DROP constraint comercio_pk; 
					      alter table compra DROP constraint compra_pk;
					      alter table rechazo DROP constraint rechazo_pk;
					      alter table cierre DROP constraint cierre_pk;
					      alter table alerta DROP constraint alerta_pk;`)
		if err != nil {
			log.Fatal(err)
		}
		main()
	}
		if input == 0 {
		
	}

}
